package com.onebyte.navigationcomponent;


import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.os.Bundle;

public class HomeActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //       Fabric.with(this, new Crashlytics());
        try {
            setContentView( R.layout.activity_home);
            context = this;

            iniViews();
            listenerCallBack();

        } catch (Exception e) {
            /** Handle Exception */
        }
    }

    private void iniViews() {
    }

    private void listenerCallBack() {
    }
}